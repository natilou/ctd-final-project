package com.example.PI_grupo_10.model.dto;

import com.example.PI_grupo_10.model.Role;

public class AuthDto {
    public String email;
    public String token;
    public String name;
    public String lastName;
    public Role role;
    public int id;
}
