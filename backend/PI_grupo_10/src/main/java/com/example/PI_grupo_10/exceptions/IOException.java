package com.example.PI_grupo_10.exceptions;

public class IOException extends Exception{
    public IOException(String message) {
        super(message);
    }
}
